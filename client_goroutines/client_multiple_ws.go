package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/jaswdr/faker"
)

type Pers struct {
	UUID    string `json:"uuid"`
	Name    string `json:"name"`
	Phone   string `json:"phone"`
	Address string `json:"address"`
	Email   string `json:"email"`
	Client  string `json:"client"`
}

var (
	wg              sync.WaitGroup
	server          string
	server_endpoint string
	port            string
	sockets_count   int
	m_count         int
	c_string        string
	min             int
	max             int
	u               url.URL
)

func init() {
	flag.StringVar(&server, "server", LookupEnvOrString("SERVER", "server"), "Websocket server hostname (or load balancer url)")
	flag.StringVar(&port, "port", LookupEnvOrString("SERVER_PORT", "8080"), "Websocket server port")
	flag.StringVar(&server_endpoint, "server_endpoint", LookupEnvOrString("SERVER_ENDPOINT", "/ws"), "Websocket server endpoint.")
	flag.IntVar(&sockets_count, "sockets_count", LookupEnvOrInt("SOCKETS_COUNT", 1), "Number of sockets to open in parallel. Works best when <= available CPU cores.")
	flag.IntVar(&m_count, "m_count", LookupEnvOrInt("MESSAGES_COUNT", 100), "Number of messages to be sent by each socket.")
	flag.IntVar(&min, "min_client", LookupEnvOrInt("MIN_CLIENT", 1), "Minimal client number to use in Faker")
	flag.IntVar(&max, "max_client", LookupEnvOrInt("MAX_CLIENT", 4), "Maximal client number to use in Faker")
	flag.Parse()
	c_string = server + ":" + port
	u = url.URL{Scheme: "ws", Host: c_string, Path: server_endpoint}
}

func LookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func LookupEnvOrInt(key string, defaultVal int) int {
	if val, ok := os.LookupEnv(key); ok {
		res, err := strconv.Atoi(val)
		if err != nil {
			log.Println("Environment variable should be Int.")
			panic(err)
			os.Exit(1)
		}
		return res
	}
	return defaultVal
}

func setSystemLimits() {
	// Increase resources limitations
	var rLimit syscall.Rlimit
	if err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rLimit); err != nil {
		panic(err)
	}
	fmt.Printf("max conn: %v \n", rLimit.Max)
	rLimit.Cur = rLimit.Max
	if err := syscall.Setrlimit(syscall.RLIMIT_NOFILE, &rLimit); err != nil {
		panic(err)
	}
}

func sendMessages(id int) {
	fmt.Printf("Client %v started \n", id)
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)
	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	//conns = append(conns, c)
	if err != nil {
		log.Fatal("dial:", err)
	}
	//defer c.Close()
	start := time.Now()
	count := 0
	f := faker.New()

	go func() {
		<-interrupt
		err = c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
		if err != nil {
			log.Println("write close:", err)
			defer wg.Done()
			return
		}
	}()

	for i := 0; i < m_count; i++ {
		pers := Pers{
			UUID:    uuid.New().String(),
			Name:    f.Person().Name(),
			Phone:   f.Phone().Number(),
			Address: f.Address().Address(),
			Email:   f.Internet().Email(),
			Client:  "clnt" + strconv.Itoa(rand.Intn(max-min+1)+min),
		}
		out, _ := json.Marshal(pers)
		err := c.WriteMessage(websocket.TextMessage, []byte(string(out)))
		count++
		elapsed := time.Since(start)
		if elapsed.Seconds() >= 1 {
			start = time.Now()
			fmt.Printf("Client %v produces %v per second \n", id, count)
			count = 0
		}
		if err != nil {
			log.Println("write:", err)
			defer wg.Done()
			return
		}
	}
	err = c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	if err != nil {
		log.Println("write close:", err)
		defer wg.Done()
		//c.Close()
		return
	}
	defer wg.Done()
}

func main() {
	setSystemLimits()

	main_start := time.Now()
	rand.Seed(time.Now().UnixNano())

	log.Printf("connecting to %s", u.String())

	for i := 0; i < sockets_count; i++ {
		wg.Add(1)
		time.Sleep(time.Millisecond * 5)
		go sendMessages(i)
	}
	wg.Wait()
	main_elapsed := time.Since(main_start)
	fmt.Printf("Time elapsed from client start %v seconds \n", int(main_elapsed.Seconds()))
	os.Exit(0)
}
