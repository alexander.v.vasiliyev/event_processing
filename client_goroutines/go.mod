module main.mod

go 1.16

require (
	github.com/google/uuid v1.2.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jaswdr/faker v1.3.0 // indirect
)
