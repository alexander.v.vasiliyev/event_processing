module main.mod

go 1.16

require (
	github.com/confluentinc/confluent-kafka-go v1.6.1 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/prometheus/client_golang v1.10.0 // indirect
)
