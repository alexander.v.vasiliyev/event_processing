package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/gorilla/websocket"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

var (
	opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "server_messages_processed",
		Help: "The total number of processed events",
	})
	kafka_server string
	k_port       string
	s_port       string
	conn_string  string
	producer     *kafka.Producer
)

func init() {
	flag.StringVar(&kafka_server, "kafka_server", LookupEnvOrString("S_KAFKA_SERVER", "kafka"), "Kafka broker hostname (or load balancer URL)")
	flag.StringVar(&k_port, "kafka_port", LookupEnvOrString("S_KAFKA_PORT", "9093"), "Kafka broker port")
	flag.StringVar(&s_port, "server_port", LookupEnvOrString("S_SERVER_PORT", "8080"), "Port number for server to listen on.")
	flag.Parse()
	conn_string = kafka_server + ":" + k_port
	producer, _ = kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": conn_string, "socket.keepalive.enable": "true"})
}

func setupRoutes() {
	http.HandleFunc("/healthcheck", healthcheck)
	http.HandleFunc("/ws", ws)
	http.HandleFunc("/reader", httpReader)
	http.Handle("/metrics", promhttp.Handler())
}

func LookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func setSystemLimits() {
	// Increase resources limitations
	var rLimit syscall.Rlimit
	if err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rLimit); err != nil {
		panic(err)
	}
	fmt.Printf("max conn: %v \n", rLimit.Max)
	rLimit.Cur = rLimit.Max
	if err := syscall.Setrlimit(syscall.RLIMIT_NOFILE, &rLimit); err != nil {
		panic(err)
	}
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "up and running")
}

func httpReader(w http.ResponseWriter, r *http.Request) {
	producer, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": conn_string, "socket.keepalive.enable": "true"})
	if err != nil {
		panic(err)
	}
	//defer producer.Close()
	topic := "default"
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	var parsed map[string]interface{}
	err = json.Unmarshal(b, &parsed)

	if err != nil {
		panic(err)
	}
	topic = string(parsed["client"].(string))
	println(topic)
	if len(topic) == 0 {
		topic = "default"
	}

	producer.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Value:          []byte(b),
	}, nil)
	opsProcessed.Inc()
	//log.Print(string(parsed.Name))

}

func reader(conn *websocket.Conn) {
	topic := "default"
	count := 0
	global_count := 0
	run := true
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGCHLD)

	for run {
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
			producer.Flush(1000)
			conn.Close()
			os.Exit(2)
		default:
			_, p, err := conn.ReadMessage()
			if err != nil {
				log.Print(err)
				producer.Flush(1000)
				conn.Close()
				return
			}

			var parsed map[string]interface{}
			err = json.Unmarshal(p, &parsed)
			if err != nil {
				log.Print(err)
			}
			topic = string(parsed["client"].(string))
			if len(topic) == 0 {
				topic = "default"
			}

			producer.Produce(&kafka.Message{
				TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
				Value:          p,
			}, nil)

			count++
			global_count++
			opsProcessed.Inc()
			if count >= 1000 {
				println(global_count)
				count = 0
			}
		}
		//log.Print(string(p))
	}
	defer conn.Close()
}

func ws(w http.ResponseWriter, r *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print(err)
	}
	log.Printf("ws connected %v", r.Host)
	reader(ws)
}

func main() {
	// Increase resources limitations
	setSystemLimits()
	fmt.Printf("Server started. Listening to %v", s_port)
	setupRoutes()
	log.Fatal(http.ListenAndServe(":"+s_port, nil))
	defer producer.Close()
}
