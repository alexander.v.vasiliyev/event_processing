# event_processing

Small pet project to practice some of the modern tech.

This project is designed to generate fake events in JSON format and then store them in the Postgres DB after processing.
Client generates JSON data, and sends it to the server via websockets. Server performs inital data processing and sends tha data to Kafka (based on "client" field messages are separated to queue per client). Group of consumers performs reading events from Kafka and saves them to the Postgres DB.

Each entity is designed to work in async way and to be able to scale if required.

On a single Macbook this solution is able to process ~10k events per second and up to 5k simultaneosly opened sockets connections. And there are a lot of room to improve.

This project could be deployed on a single instance using Docker and docker-compose - `docker-compose up --build`. 

Or it can be deployed to AWS EKS using terraform and helm. The single command `terraform apply` will create all required infrastructure and will run helm charts to deploy the apps. (apps containers should be availiable to download by the cluster).

The solution is able to self healing.

Technologies used:
- golang
- Docker
- Terraform
- K8S
- Helm
- Kafka
- Postgres
- Python and Javascript for testing purposes
