package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/jaswdr/faker"
)

type Pers struct {
	UUID    string `json:"uuid"`
	Name    string `json:"name"`
	Phone   string `json:"phone"`
	Address string `json:"address"`
	Email   string `json:"email"`
	Client  string `json:"client"`
}

func sendMessages(wg *sync.WaitGroup, id int, serverUrl string, messages int, min int, max int) {
	client := &http.Client{}
	f := faker.New()
	count := 0
	start := time.Now()
	for i := 0; i < 100; i++ {
		pers := Pers{
			UUID:    uuid.New().String(),
			Name:    f.Person().Name(),
			Phone:   f.Phone().Number(),
			Address: f.Address().Address(),
			Email:   f.Internet().Email(),
			Client:  "clnt" + strconv.Itoa(rand.Intn(max-min+1)+min),
		}
		out, _ := json.Marshal(pers)

		var jsonStr = []byte(out)
		req, err := http.NewRequest("POST", serverUrl, bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")

		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		count++
		elapsed := time.Since(start)
		//print(elapsed.Seconds())
		if elapsed.Seconds() >= 1 {
			start = time.Now()
			println(count)
			count = 0
		}
		//fmt.Println("response Status:", resp.Status)
		//fmt.Println("response Headers:", resp.Header)
		//body, _ := ioutil.ReadAll(resp.Body)
		//fmt.Println("response Body:", string(body))
		defer resp.Body.Close()
	}
	defer wg.Done()
}

func main() {
	main_start := time.Now()
	url := "http://localhost:8080/reader"
	fmt.Println("URL:>", url)

	rand.Seed(time.Now().UnixNano())
	min := 1
	max := 4

	var wg sync.WaitGroup
	for i := 0; i < 1; i++ {
		wg.Add(1)
		go sendMessages(&wg, i, url, 10000, min, max)
	}
	wg.Wait()
	main_elapsed := time.Since(main_start)
	println(int(main_elapsed.Seconds()))
}
