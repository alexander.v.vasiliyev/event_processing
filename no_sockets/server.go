package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Pers struct {
	UUID    string `json:"uuid"`
	Name    string `json:"name"`
	Phone   string `json:"phone"`
	Address string `json:"address"`
	Email   string `json:"email"`
	Client  string `json:"client"`
}

var (
	opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "server_messages_processed",
		Help: "The total number of processed events",
	})
)

//var kafka_server = os.Args[1]
//var k_port = os.Args[2]
//var s_port = os.Args[3]
//var conn_string = kafka_server + ":" + k_port

func healthcheck(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "test")
}

func reader(w http.ResponseWriter, r *http.Request) {
	//producer, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": conn_string, "socket.keepalive.enable": "true"})
	//if err != nil {
	//	panic(err)
	//}
	//defer producer.Close()
	topic := "default"
	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	var parsed Pers
	err = json.Unmarshal(b, &parsed)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err != nil {
		panic(err)
	}
	topic = string(parsed.Client)
	if len(topic) == 0 {
		topic = "default"
	}

	//producer.Produce(&kafka.Message{
	//	TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
	//	Value:          r.Body,
	//}, nil)
	opsProcessed.Inc()
	//log.Print(string(parsed.Name))

}

func setupRoutes() {
	http.HandleFunc("/healthcheck", healthcheck)
	http.HandleFunc("/reader", reader)
	http.Handle("/metrics", promhttp.Handler())
}

func main() {
	fmt.Println("test")
	//if len(os.Args) < 4 {
	//	fmt.Fprintf(os.Stderr, "Usage: %s <kafka> <kafka port> <server port>\n",
	//		os.Args[0])
	//	os.Exit(1)
	//}

	setupRoutes()
	log.Fatal(http.ListenAndServe(":8081", nil))
}
