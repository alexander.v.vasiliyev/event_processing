provider "aws" {
  region = "eu-west-1"
}

provider "helm" {
  kubernetes {
    config_path = "./kubeconfig_my-cluster"
  }
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.9"
}

data "aws_availability_zones" "available" {
}

locals {
  cluster_name = "my-cluster"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.77.0"

  name                 = "k8s-vpc"
  cidr                 = "172.16.0.0/16"
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = ["172.16.1.0/24", "172.16.2.0/24", "172.16.3.0/24"]
  public_subnets       = ["172.16.4.0/24", "172.16.5.0/24", "172.16.6.0/24"]
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  #version = "12.2.0"

  cluster_name    = local.cluster_name
  cluster_version = "1.19"
  subnets         = module.vpc.private_subnets

  vpc_id = module.vpc.vpc_id

  node_groups = {
    first = {
      desired_capacity = 3
      max_capacity     = 5
      min_capacity     = 1

      instance_type = "t3a.large"
    }
  }

  write_kubeconfig   = true
  config_output_path = "./"
}

resource "helm_release" "kafka" {
  name       = "kafka"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "bitnami/kafka"

  values = [
    file("${path.module}/kafka/values.yaml")
  ]
  depends_on = [module.eks]
}

resource "helm_release" "postgres" {
  name       = "db"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "postgresql"

  values = [
    file("${path.module}/postgres/values.yaml")
  ]
  depends_on = [module.eks]
}

resource "helm_release" "prometheus" {
  name       = "prometheus"

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus"
  depends_on = [module.eks]
}

//get generated password
//kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
resource "helm_release" "grafana" {
  name       = "grafana"

  repository = "https://grafana.github.io/helm-charts"
  chart      = "grafana"
  depends_on = [module.eks]
}

resource "helm_release" "server" {
  name       = "server"
  chart      = "./helm/server"
  depends_on = [module.eks,helm_release.kafka]
}

resource "helm_release" "consumer" {
  name       = "consumer"
  chart      = "./helm/consumer"
  depends_on = [module.eks,helm_release.kafka, helm_release.postgres]
}