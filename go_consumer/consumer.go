// Example function-based high-level Apache Kafka consumer
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"github.com/go-playground/validator"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Pers struct {
	UUID    string `json:"uuid" validate:"required"`
	Name    string `json:"name"`
	Phone   string `json:"phone"`
	Address string `json:"address"`
	Email   string `json:"email"`
	Client  string `json:"client"`
}

func init() {
	flag.StringVar(&kafka_server, "kafka_server", LookupEnvOrString("KAFKA_SERVER", "kafka"), "Kafka broker hostname (or load balancer URL)")
	flag.StringVar(&kafka_group, "kafka_group", LookupEnvOrString("KAFKA_GROUP", "group1"), "Kafka consumers group name")
	flag.StringVar(&db_addr, "db_addr", LookupEnvOrString("DB_ADDR", "db:5432"), "Database address and port.")
	flag.StringVar(&db_username, "db_username", LookupEnvOrString("DB_USERNAME", "postgres"), "Database address and port.")
	flag.StringVar(&db_password, "db_password", LookupEnvOrString("DB_PASSWORD", "postgres"), "Database password")
	flag.StringVar(&database_name, "database_name", LookupEnvOrString("DATABASE_NAME", "postgres"), "Database name.")
	flag.StringVar(&topic, "topic", LookupEnvOrString("KAFKA_TOPIC", "default"), "Kafka topic name to listen.")
	flag.Parse()
	producer, _ = kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": kafka_server, "socket.keepalive.enable": "true"})
	c, _ = kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": kafka_server,
		// Avoid connecting to IPv6 brokers:
		// This is needed for the ErrAllBrokersDown show-case below
		// when using localhost brokers on OSX, since the OSX resolver
		// will return the IPv6 addresses first.
		// You typically don't need to specify this configuration property.
		"broker.address.family":           "v4",
		"group.id":                        kafka_group,
		"session.timeout.ms":              6000,
		"go.application.rebalance.enable": true,
		// Enable generation of PartitionEOF when the
		// end of a partition is reached.
		"enable.partition.eof": true,
		"auto.offset.reset":    "earliest"})
	validate = validator.New()
}

func LookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

var (
	opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "consumer_messages_processed",
		Help: "The total number of processed events",
	})
	opsDlq = promauto.NewCounter(prometheus.CounterOpts{
		Name: "consumer_messages_sent_to_dlq",
		Help: "The total number of broken events",
	})
	validate      *validator.Validate
	kafka_server  string
	kafka_group   string
	db_addr       string
	db_username   string
	db_password   string
	database_name string
	topic         string
	producer      *kafka.Producer
	c             *kafka.Consumer
	db            *pg.DB
)

func connect_to_db(user string, db_addr string) *pg.DB {
	db := pg.Connect(&pg.Options{
		Addr:     db_addr,
		User:     db_username,
		Password: db_password,
		Database: database_name,
	})

	err := createSchema(db)
	if err != nil {
		println(err.Error())
	}
	return db
}

func createSchema(db *pg.DB) error {
	models := []interface{}{
		(*Pers)(nil),
	}

	for _, model := range models {
		err := db.Model(model).CreateTable(&orm.CreateTableOptions{})
		if err != nil {
			return err
		}
	}
	return nil
}

func validateMessage(message Pers) error {
	err := validate.Struct(message)
	if err != nil {
		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println(err)
			return err
		}

		for _, err := range err.(validator.ValidationErrors) {

			fmt.Println(err)
		}
		// from here you can create your own error messages in whatever language you wish
		return err
	}
	return nil
}

func sendToDlq(message []byte, producer kafka.Producer) {
	topic := "dlq"
	var parsed map[string]interface{}
	_ = json.Unmarshal(message, &parsed)
	producer.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Value:          message,
	}, nil)
	opsDlq.Inc()
	fmt.Printf("message %v was sent to the \"dlq\" topic \n", parsed)
}

func write_to_db(db *pg.DB, message []byte, producer kafka.Producer) {
	var pers Pers
	err := json.Unmarshal(message, &pers)
	if err != nil {
		panic(err)
	}
	println(pers.Name)
	err = validateMessage(pers)
	if err != nil {
		log.Println(err)
		sendToDlq(message, producer)
		return
	}

	_, err = db.Model(&pers).Insert()
	if err != nil {
		log.Println("PANIC CANT SAVE!!!", err)
		sendToDlq(message, producer)
		return
	}
	opsProcessed.Inc()

}

func readAndSave() {
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGCHLD)
	run := true
	for run {
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			ev := c.Poll(100)
			if ev == nil {
				continue
			}

			switch e := ev.(type) {
			case *kafka.Message:
				//fmt.Printf("%% Message on %s:\n%s\n",
				//	e.TopicPartition, string(e.Value))
				write_to_db(db, e.Value, *producer)
				if e.Headers != nil {
					fmt.Printf("%% Headers: %v\n", e.Headers)
				}
			case kafka.Error:
				// Errors should generally be considered
				// informational, the client will try to
				// automatically recover.
				// But in this example we choose to terminate
				// the application if all brokers are down.
				fmt.Fprintf(os.Stderr, "%% Error: %v: %v\n", e.Code(), e)
				if e.Code() == kafka.ErrAllBrokersDown {
					run = false
				}
			default:
				//fmt.Printf("Ignored %v\n", e)
			}
		}
	}
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "up and running")
}

func setupRoutes() {
	http.HandleFunc("/healthcheck", healthcheck)
	http.Handle("/metrics", promhttp.Handler())
}

func main() {

	db = connect_to_db(db_username, db_addr)

	fmt.Printf("Created Consumer %v\n", c)

	err := c.Subscribe(topic, nil)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to subscribe to the topic: %s\n", err)
		os.Exit(1)
	}

	setupRoutes()
	go http.ListenAndServe(":8080", nil)
	readAndSave()

	fmt.Printf("Closing consumer\n")
	c.Close()
	defer db.Close()
}
