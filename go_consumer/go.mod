module main.mod

go 1.16

require (
	github.com/confluentinc/confluent-kafka-go v1.6.1 // indirect
	github.com/go-pg/pg/v10 v10.9.0 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/prometheus/client_golang v1.10.0 // indirect
)
